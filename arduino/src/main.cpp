#include <ClickEncoder.h>
#include <Task.h>
#include <FastLED.h>
#include <Chrono.h>
#include "display.h"
#include "timer.h"

#define CLICK_PIN 3
#define PIN_A 4
#define PIN_B 5

Chrono chrono(Chrono::SECONDS);

ClickEncoder *encoder;

void timerIsr(uint32_t delta) {
    encoder->service();
}

Display *display;
Timer *timer;
TaskManager taskManager;
FunctionTask encoderService(timerIsr, MsToTaskTime(1));

void setup() {
    
    Serial.begin(9600);

    display = new Display();
    encoder = new ClickEncoder(PIN_A, PIN_B, CLICK_PIN);
    encoder->setAccelerationEnabled(false);
    taskManager.StartTask(&encoderService);

    timer = new Timer(display, encoder, &chrono);
}

void loop() {  
    taskManager.Loop();
    timer->update();
}
