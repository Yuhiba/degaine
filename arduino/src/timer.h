#ifndef __have__Timer_h__
#define __have__Timer_h__

#include <Chrono.h>
#include <ClickEncoder.h>
#include "display.h"


class Timer {
    public:
        typedef struct {
            CRGB running_start = CRGB::Green;
            CRGB running_mid = CRGB::Blue;
            CRGB running_end = CRGB::Yellow;;
            CRGB running_finish = CRGB::Red;
            CRGB stopped = CHSV(240, 255, 255);
            CRGB paused = CRGB::OrangeRed;
            CRGB standby = CHSV(240, 255, 150);
        } Colors;

        Timer(Display *display, ClickEncoder *encoder, Chrono *chrono);
        void update();

    private:
        Display *display;
        ClickEncoder *encoder;
        Chrono *chrono;
        Colors colors;
};

#endif